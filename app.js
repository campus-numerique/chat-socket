var express = require('express.io');
var app = express().http().io();
var bodyParser = require("body-parser");
var port = process.env.PORT || 1983;

// Setup your sessions, just like normal.
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.cookieParser());
app.use(express.session({secret: 'monkey'}));
app.use(express.static('public'));

var usersConnected = [];
var hist = new Array; //Histo of chat
var point = 0;


function balisepas (string) {
    var chaine = string.split('');
    for (i=0; i < chaine.length; i++) {
        if (chaine[i] == '<') {
            chaine[i] = '&lsaquo;';
        }
        if (chaine[i] == '>') {
            chaine[i] = '&rsaquo;';
        }
    }
    string = chaine.join('');
    return string;
};


app.io.on('connection', function(socket){
    console.log('user is connect');

    socket.on('disconnect', function(){
        console.log('user is disconnect');
        

        var UUId = socket.manager.handshaken[this.id].sessionID;
        for (var i = 0; i < usersConnected.length; i++) {
            
            if (usersConnected[i].id === UUId) {

                usersConnected.splice(i, 1);
            }
        }

        app.io.broadcast('connectUser', usersConnected);
    });
});
// Session is automatically setup on initial request.
app.get('/', function(req, res) {
    res.sendfile(__dirname + '/index.html');
})
.get('/campus', function(req, res){

   var bool = false;

   for (var i =0; i < usersConnected.length; i++) {         
        if ( usersConnected[i].pseudo === req.session.name && req.session.name.length < 30) {
            bool = true;        
        }
    }
    if(bool){ 
        res.sendfile(__dirname + '/client.html');
    }
    else{
        console.log("error");
        res.redirect('/');
    }
    
})
.post('/login', function(req,res){
     req.session.name = balisepas(req.body.pseudo);
     req.session.color = req.body.color;
    console.log("Laaaa !!! IIICIII JE SUIS LAAAAAAAA" , req.session);
    var userName = req.session.name;
    var idSessionUser =  req.session.id;
    var userConnection = new Object();
        userConnection.id = idSessionUser;
        userConnection.pseudo = userName;
        userConnection.color = req.body.color;
        usersConnected[usersConnected.length] = userConnection;
        console.log('connectUser', usersConnected);
    app.io.broadcast('connectUser', usersConnected);    
    res.redirect('/campus');
})
//gestion des erreurs
.use(function(req,res){
	res.sendfile(__dirname + '/404.html'); 
});

  

// Setup a route for the ready event, and add session data.
app.io.route('ready', function(req, res) {
    var hist2 = {historique:hist, point:point};

    app.io.broadcast('connectUser', usersConnected);
    
    req.session.save(function() {
        req.io.emit('serveur_chat_message', req.session.name);
        req.io.emit('displayHist', hist2);        
    });
});


// Send back the session data.
app.io.route('user_chat_message', function(req) {
    console.log(req.data);
    req.session.feelings = req.data;
    console.log(req.session.color);
    var messag = {pseudo:req.session.name, content: req.data, color:req.session.color }
    hist[point] = messag;
    point++;
    if (point > 9) {
        point = 0;
    }
    console.log(hist);


    req.session.save(function() {
        app.io.broadcast('session', req.session);
    });
});

app.listen(port, console.log("j'ecoute le 1983"));